package org.apache.jsp.admin;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class habitacion_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(1);
    _jspx_dependants.add("/admin/menu.jsp");
  }

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <style>\n");
      out.write("            *{\n");
      out.write("                font-family: 'Manjari', sans-serif !important;\n");
      out.write("\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("        <!--calendario -->\n");
      out.write("\n");
      out.write("        <link href=\"css/bootstrap-datetimepicker.min.css\" rel=\"stylesheet\">\n");
      out.write("        <script src=\"js/bootstrap-datetimepicker.min.js\"></script>\n");
      out.write("        <!--calendario -->\n");
      out.write("\n");
      out.write("        <link href=\"https://fonts.googleapis.com/css?family=Manjari&display=swap\" rel=\"stylesheet\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n");
      out.write("        <link type=\"text/css\" rel=\"stylesheet\" link=\"../bootstrap/css/bootstrap.min.css\"/>\n");
      out.write("        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <title>Habitacion</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        ");
      out.write("<style>\n");
      out.write("    .nav>.nav-link{\n");
      out.write("        margin:25px 15px 15px 15px;\n");
      out.write("        color: #fff;\n");
      out.write("        \n");
      out.write("    }\n");
      out.write("    a:hover{\n");
      out.write("        transform: scale(1.1);\n");
      out.write("    }\n");
      out.write("</style>\n");
      out.write("\n");
      out.write("<nav class=\"nav justify-content-center \" style=\"background-color:#079992\">\n");
      out.write("    <a class=\"nav-link\" href=\"index.jsp\">INICIO</a>\n");
      out.write("    <a class=\"nav-link\" href=\"habitacion.jsp\">HABITACIÓN</a>\n");
      out.write("    <h3 display-3 class=\"nav-link\">HOTEL</h3>\n");
      out.write("    <a class=\"nav-link\" href=\"#\">XXXXX</a>\n");
      out.write("    <a class=\"nav-link\" href=\"#\">XXXXX</a>\n");
      out.write("</nav>");
      out.write("\n");
      out.write("\n");
      out.write("        <div class=\"section\" style=\"padding-top:25px\">\n");
      out.write("            <h3 class=\"header\" align=\"center\" style=\"color:#079992\">Cargar habitación</h3>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <form action=\"guardarHabitacion\" method=\"post\">\n");
      out.write("                <div class=\"form-row\">\n");
      out.write("                    <div class=\"form-group col-md-4\">\n");
      out.write("                        <label>Número de habitación</label>\n");
      out.write("                        <input type=\"number\" class=\"form-control\" name=\"numeroHabitacion\" placeholder=\"N° habitación\">\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group col-md-4\">\n");
      out.write("                        <label>Piso</label>\n");
      out.write("                        <select class=\"form-control\" name=\"piso\">\n");
      out.write("                            <option slected>Seleccionar</option>\n");
      out.write("                            <option value=\"1\">Uno</option>\n");
      out.write("                            <option value=\"2\">Dos</option>\n");
      out.write("                            <option value=\"3\">Tres</option>\n");
      out.write("                            <option value=\"4\">Cuatro</option>\n");
      out.write("                            <option value=\"5\">Cinco</option>\n");
      out.write("                            <option value=\"6\">Seis</option>\n");
      out.write("                        </select>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group col-md-4\">\n");
      out.write("                        <label>Precio por noche</label>\n");
      out.write("                        <div class=\"input-group\">\n");
      out.write("                            <div class=\"input-group-prepend\">\n");
      out.write("                                <div class=\"input-group-text\">$</div>\n");
      out.write("                            </div>\n");
      out.write("                            <input type=\"number\" class=\"form-control\" placeholder=\"Precio\">\n");
      out.write("                        </div>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"form-group col-md-12\">\n");
      out.write("                        <label>Descripción de la habitación</label>\n");
      out.write("                        <textarea name=\"descripcion\" placeholder=\"\" class=\"form-control\" rows=\"4\" style=\"max-height:100px\" maxlength=\"300\"></textarea>\n");
      out.write("                        <small  class=\"text-muted\">\n");
      out.write("                            Máximo 300 caracteres.\n");
      out.write("                        </small>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"form-group col-md-4\">\n");
      out.write("                        <label>Tipo de habitación</label>\n");
      out.write("                        <select class=\"form-control\">\n");
      out.write("                            <option selected>Elegir tipo</option>\n");
      out.write("                            <option value=\"Standar\">Standar</option>\n");
      out.write("                            <option value=\"Ejecutiva\">Ejecutiva</option>\n");
      out.write("                            <option value=\"Ejecutiva Familiar\">Ejecutiva Familiar</option>\n");
      out.write("                            <option value=\"Suite\">Suite</option>\n");
      out.write("                            <option value=\"Master\">Master Suite</option>\n");
      out.write("                        </select>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"col-md-12\">\n");
      out.write("                        <button type=\"submit\" class=\"btn btn-outline-primary btn-lg\">Guardar habitación</button>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("                </div>\n");
      out.write("\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("    </form>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
