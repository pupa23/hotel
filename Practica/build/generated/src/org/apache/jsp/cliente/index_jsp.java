package org.apache.jsp.cliente;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  static {
    _jspx_dependants = new java.util.ArrayList<String>(2);
    _jspx_dependants.add("/cliente/../conexion.jsp");
    _jspx_dependants.add("/cliente/menu.jsp");
  }

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_sql_query_var_dataSource;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_sql_query_var_dataSource = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_sql_query_var_dataSource.release();
    _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('\n');
      out.write('\n');
      if (_jspx_meth_sql_setDataSource_0(_jspx_page_context))
        return;
      out.write(" \n");
      out.write("\n");
      if (_jspx_meth_sql_query_0(_jspx_page_context))
        return;
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("\n");
      out.write("        <style>\n");
      out.write("            *{\n");
      out.write("                font-family: 'Manjari', sans-serif !important;\n");
      out.write("\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("        <!--calendario -->\n");
      out.write("\n");
      out.write("        <link href=\"css/bootstrap-datetimepicker.min.css\" rel=\"stylesheet\">\n");
      out.write("        <script src=\"js/bootstrap-datetimepicker.min.js\"></script>\n");
      out.write("        <!--calendario -->\n");
      out.write("\n");
      out.write("        <link href=\"https://fonts.googleapis.com/css?family=Manjari&display=swap\" rel=\"stylesheet\">\n");
      out.write("        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"/>\n");
      out.write("        <link type=\"text/css\" rel=\"stylesheet\" link=\"../bootstrap/css/bootstrap.min.css\"/>\n");
      out.write("        <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>\n");
      out.write("        <title>Index</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        ");
      out.write("<style>\n");
      out.write("    .nav>.nav-link{\n");
      out.write("        margin:25px 15px 15px 15px;\n");
      out.write("        color: #fff;\n");
      out.write("        \n");
      out.write("    }\n");
      out.write("    a:hover{\n");
      out.write("        transform: scale(1.1);\n");
      out.write("    }\n");
      out.write("</style>\n");
      out.write("\n");
      out.write("<nav class=\"nav justify-content-center \" style=\"background-color:#079992\">\n");
      out.write("    <a class=\"nav-link\" href=\"index.jsp\">INICIO</a>\n");
      out.write("    <a class=\"nav-link\" href=\"#\">CONOZCA NUESTRAS INSTALACIONES</a>\n");
      out.write("    <h3 display-3 class=\"nav-link\">HOTEL</h3>\n");
      out.write("    <a class=\"nav-link\" href=\"#\">CONTÁCTENOS</a>\n");
      out.write("    <a class=\"nav-link\" href=\"#\">CONTÁCTENOS</a>\n");
      out.write("</nav>");
      out.write(" \n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("        <div id=\"carouselExampleFade\" class=\"carousel slide carousel-fade\" data-ride=\"carousel\">\n");
      out.write("            <div class=\"carousel-inner\">\n");
      out.write("                <div class=\"carousel-item active\">\n");
      out.write("                    <img src=\"../imagenes/1.jpg\" class=\"d-block w-100\" alt=\"...\" style=\"height: 500px\">\n");
      out.write("                </div>\n");
      out.write("                <div class=\"carousel-item\">\n");
      out.write("                    <img src=\"../imagenes/habitacion1.jpg\" class=\"d-block w-100\" alt=\"...\" style=\"height: 500px\">\n");
      out.write("                </div>\n");
      out.write("                <div class=\"carousel-item\">\n");
      out.write("                    <img src=\"../imagenes/habitacion2.jpg\" class=\"d-block w-100\" alt=\"...\" style=\"height: 500px\">\n");
      out.write("                </div>\n");
      out.write("            </div>\n");
      out.write("            <a class=\"carousel-control-prev\" href=\"#carouselExampleFade\" role=\"button\" data-slide=\"prev\">\n");
      out.write("                <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>\n");
      out.write("                <span class=\"sr-only\">Previous</span>\n");
      out.write("            </a>\n");
      out.write("            <a class=\"carousel-control-next\" href=\"#carouselExampleFade\" role=\"button\" data-slide=\"next\">\n");
      out.write("                <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\n");
      out.write("                <span class=\"sr-only\">Next</span>\n");
      out.write("            </a>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <div class=\"section\" style=\"padding-top:15px\">\n");
      out.write("            <h3 class=\"header\" align=\"center\" style=\"color:salmon\">Reservar</h3>\n");
      out.write("        </div>\n");
      out.write("        <hr>\n");
      out.write("        <div class=\"container\">\n");
      out.write("            <form action=\"recibir.jsp\" method=\"POST\">\n");
      out.write("                <div class=\"row\">\n");
      out.write("                    <div class=\"col-4\">\n");
      out.write("                        <h5>Cantidad de niños</h5>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-8\">\n");
      out.write("                        <select class=\"custom-select pull-right\" name=\"menores\" >\n");
      out.write("                            <option value=\"0\"></option>\n");
      out.write("                            <option value=\"1\">Uno</option>\n");
      out.write("                            <option value=\"2\">Dos</option>\n");
      out.write("                            <option value=\"3\">Tres</option>\n");
      out.write("                            <option value=\"4\">Cuatro</option>\n");
      out.write("                            <option value=\"5\">Cinco</option>\n");
      out.write("                            <option value=\"6\">Seis</option>\n");
      out.write("                        </select>\n");
      out.write("                    </div>\n");
      out.write("                    <br>\n");
      out.write("                    <br>\n");
      out.write("\n");
      out.write("                    <div class=\"col-4\">\n");
      out.write("                        <h5>Cantidad de adultos</h5>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-8 \">\n");
      out.write("                        <select class=\"custom-select pull-right\" name=\"adultos\">\n");
      out.write("                            \n");
      out.write("                            <option value=\"1\">Uno</option>\n");
      out.write("                            <option value=\"2\">Dos</option>\n");
      out.write("                            <option value=\"3\">Tres</option>\n");
      out.write("                            <option value=\"4\">Cuatro</option>\n");
      out.write("                            <option value=\"5\">Cinco</option>\n");
      out.write("                            <option value=\"6\">Seis</option>\n");
      out.write("                        </select>\n");
      out.write("                    </div>\n");
      out.write("                    <br>\n");
      out.write("                    <br>\n");
      out.write("                    <div class=\"col-4\">\n");
      out.write("                        <h5>Fecha de llegada </h5>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-8\">\n");
      out.write("                        <input type=\"date\" class=\"form-control\" name=\"check-in\">\n");
      out.write("                    </div>\n");
      out.write("                    <br>\n");
      out.write("                    <br>\n");
      out.write("                    <div class=\"col-4\">\n");
      out.write("                        <h5>Fecha de salida </h5>\n");
      out.write("                    </div>\n");
      out.write("                    <div class=\"col-8\">\n");
      out.write("                        <input  type=\"date\" class=\"form-control\" name=\"check-out\">\n");
      out.write("                    </div> \n");
      out.write("                    <br>\n");
      out.write("                    <br>\n");
      out.write("                    <div class=\"col-4\">\n");
      out.write("                        <h5>Tipo de habitación </h5>\n");
      out.write("                    </div>\n");
      out.write("\n");
      out.write("                    <div class=\"col-8\">\n");
      out.write("                        <select class=\"custom-select pull-right\" name=\"tipo-habitacion\">\n");
      out.write("                            <option value=\"Standar\">Standar</option>\n");
      out.write("                            <option value=\"Ejecutiva\">Ejecutiva</option>\n");
      out.write("                            <option value=\"Ejecutiva Familiar\">Ejecutiva Familiar</option>\n");
      out.write("                            <option value=\"Suite\">Suite</option>\n");
      out.write("                            <option value=\"Master\">Master Suite</option>\n");
      out.write("                        </select>\n");
      out.write("                    </div> \n");
      out.write("                </div>\n");
      out.write("                <br>\n");
      out.write("                    <br>\n");
      out.write("                <div class=\"col-md-3 offset-md-6\">\n");
      out.write("                    <button type=\"submit\" class=\"btn btn-outline-primary btn-lg\">Reservar</button>\n");
      out.write("                </div>\n");
      out.write("               \n");
      out.write("            </form>\n");
      out.write("        </div>\n");
      out.write("\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("        <br>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("\n");
      out.write("\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_sql_setDataSource_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sql:setDataSource
    org.apache.taglibs.standard.tag.rt.sql.SetDataSourceTag _jspx_th_sql_setDataSource_0 = (org.apache.taglibs.standard.tag.rt.sql.SetDataSourceTag) _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody.get(org.apache.taglibs.standard.tag.rt.sql.SetDataSourceTag.class);
    _jspx_th_sql_setDataSource_0.setPageContext(_jspx_page_context);
    _jspx_th_sql_setDataSource_0.setParent(null);
    _jspx_th_sql_setDataSource_0.setDriver("com.mysql.jdbc.Driver");
    _jspx_th_sql_setDataSource_0.setUrl("jdbc:mysql://localhost/hotel");
    _jspx_th_sql_setDataSource_0.setUser("root");
    _jspx_th_sql_setDataSource_0.setPassword("gunsnroses1992");
    _jspx_th_sql_setDataSource_0.setVar("cnx");
    int _jspx_eval_sql_setDataSource_0 = _jspx_th_sql_setDataSource_0.doStartTag();
    if (_jspx_th_sql_setDataSource_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody.reuse(_jspx_th_sql_setDataSource_0);
      return true;
    }
    _jspx_tagPool_sql_setDataSource_var_user_url_password_driver_nobody.reuse(_jspx_th_sql_setDataSource_0);
    return false;
  }

  private boolean _jspx_meth_sql_query_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  sql:query
    org.apache.taglibs.standard.tag.rt.sql.QueryTag _jspx_th_sql_query_0 = (org.apache.taglibs.standard.tag.rt.sql.QueryTag) _jspx_tagPool_sql_query_var_dataSource.get(org.apache.taglibs.standard.tag.rt.sql.QueryTag.class);
    _jspx_th_sql_query_0.setPageContext(_jspx_page_context);
    _jspx_th_sql_query_0.setParent(null);
    _jspx_th_sql_query_0.setDataSource((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${cnx}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    _jspx_th_sql_query_0.setVar("consulta");
    int[] _jspx_push_body_count_sql_query_0 = new int[] { 0 };
    try {
      int _jspx_eval_sql_query_0 = _jspx_th_sql_query_0.doStartTag();
      if (_jspx_eval_sql_query_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        if (_jspx_eval_sql_query_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
          out = _jspx_page_context.pushBody();
          _jspx_push_body_count_sql_query_0[0]++;
          _jspx_th_sql_query_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
          _jspx_th_sql_query_0.doInitBody();
        }
        do {
          out.write("\n");
          out.write("    select * from persona;\n");
          int evalDoAfterBody = _jspx_th_sql_query_0.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
        if (_jspx_eval_sql_query_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
          out = _jspx_page_context.popBody();
          _jspx_push_body_count_sql_query_0[0]--;
      }
      if (_jspx_th_sql_query_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        return true;
      }
    } catch (Throwable _jspx_exception) {
      while (_jspx_push_body_count_sql_query_0[0]-- > 0)
        out = _jspx_page_context.popBody();
      _jspx_th_sql_query_0.doCatch(_jspx_exception);
    } finally {
      _jspx_th_sql_query_0.doFinally();
      _jspx_tagPool_sql_query_var_dataSource.reuse(_jspx_th_sql_query_0);
    }
    return false;
  }
}
