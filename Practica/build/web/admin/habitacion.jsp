<%-- 
    Document   : habitacion
    Created on : 07/10/2019, 14:53:49
    Author     : jesu7
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <style>
            *{
                font-family: 'Manjari', sans-serif !important;

            }
        </style>
        <!--calendario -->

        <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <script src="js/bootstrap-datetimepicker.min.js"></script>
        <!--calendario -->

        <link href="https://fonts.googleapis.com/css?family=Manjari&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link type="text/css" rel="stylesheet" link="../bootstrap/css/bootstrap.min.css"/>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <title>Habitacion</title>
    </head>
    <body>
        <%@include file="menu.jsp" %>

        <div class="section" style="padding-top:25px">
            <h3 class="header" align="center" style="color:#079992">Cargar habitación</h3>
        </div>

        <div class="container">
            <form action="guardarHabitacion" method="post">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Número de habitación</label>
                        <input type="number" class="form-control" name="numeroHabitacion" placeholder="N° habitación">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Piso</label>
                        <select class="form-control" name="piso">
                            <option slected>Seleccionar</option>
                            <option value="1">Uno</option>
                            <option value="2">Dos</option>
                            <option value="3">Tres</option>
                            <option value="4">Cuatro</option>
                            <option value="5">Cinco</option>
                            <option value="6">Seis</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Precio por noche</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">$</div>
                            </div>
                            <input type="number" class="form-control" placeholder="Precio">
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Descripción de la habitación</label>
                        <textarea name="descripcion" placeholder="" class="form-control" rows="4" style="max-height:100px" maxlength="300"></textarea>
                        <small  class="text-muted">
                            Máximo 300 caracteres.
                        </small>
                    </div>

                    <div class="form-group col-md-4">
                        <label>Tipo de habitación</label>
                        <select class="form-control">
                            <option selected>Elegir tipo</option>
                            <option value="Standar">Standar</option>
                            <option value="Ejecutiva">Ejecutiva</option>
                            <option value="Ejecutiva Familiar">Ejecutiva Familiar</option>
                            <option value="Suite">Suite</option>
                            <option value="Master">Master Suite</option>
                        </select>
                    </div>

                    <div class="col-md-12">
                        <button type="submit" class="btn btn-outline-primary btn-lg">Guardar habitación</button>
                    </div>


                </div>


        </div>

    </form>
</div>

</body>
</html>
