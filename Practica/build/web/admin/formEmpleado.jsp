
<form class="form-horizontal" method="POST" action="guardarEmpleado.jsp" >
  <div class="form-group">
    <label for="usuario" class="col-sm-2 control-label">Nombre de usuario</label>
    <div class="col-sm-9">
      <input type="text" id="usuario" class="form-control" name="usuario" placeholder="Usuario" required>
    </div>
  </div>
  <div class="form-group">
    <label for="password" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-9">
      <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
    </div>
  </div>
  <div class="form-group">
      <label for="acceso"  class="col-sm-2 control-label">Acceso</label>
    <div class="col-sm-9">
      <input type="text" class="form-control" id="acceso" name="acceso" placeholder="Tipo de acceso" required>
    </div>
  </div>
    
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary">Guardar</button>
    </div>
  </div>
</form>
