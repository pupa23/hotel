
<%@include file="../conexion.jsp" %>

<sql:update dataSource="${cnx}" var="result">
    INSERT INTO empleado(acceso, usuario, password) VALUES(?,?,?);
    <sql:param value="${param.acceso}" />
    <sql:param value="${param.usuario}" />
    <sql:param value="${param.password}" />
</sql:update>

<html>
    <head>
        <link href="https://fonts.googleapis.com/css?family=Manjari&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="../css/style.css">
        <link type="text/css" rel="stylesheet" href="../bootstrap/css/bootstrap.css"/>
    </head>
    <body>
        <h2 class="text-center" style="margin-top:10%">LOS DATOS FUERON GUARDADOS CON �XITO</h2>

        <div id="circle">
            <div class="loader">
                <div class="loader">
                    <div class="loader">
                        <div class="loader">

                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </body>
</html>

<meta http-equiv="Refresh" content="1;url=index.jsp">
