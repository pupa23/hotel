<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>

        <style>
            *{
                font-family: 'Manjari', sans-serif !important;

            }
        </style>
        <!--calendario -->

        <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <script src="js/bootstrap-datetimepicker.min.js"></script>
        <!--calendario -->

        <link href="https://fonts.googleapis.com/css?family=Manjari&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link type="text/css" rel="stylesheet" link="../bootstrap/css/bootstrap.min.css"/>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <title>Index</title>
    </head>
    <body>
        <%@ include file="menu.jsp" %> 



        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="../imagenes/1.jpg" class="d-block w-100" alt="..." style="height: 500px">
                </div>
                <div class="carousel-item">
                    <img src="../imagenes/habitacion1.jpg" class="d-block w-100" alt="..." style="height: 500px">
                </div>
                <div class="carousel-item">
                    <img src="../imagenes/habitacion2.jpg" class="d-block w-100" alt="..." style="height: 500px">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="section" style="padding-top:15px">
            <h3 class="header" align="center" style="color:#079992">Reservar</h3>
        </div>
        <hr>
        <div class="container">
            <form action="recibir.jsp" method="POST">
                <div class="row">
                    <div class="col-4">
                        <h5>Cantidad de niños</h5>
                    </div>
                    <div class="col-8">
                        <select class="custom-select pull-right" name="menores" >
                            <option value="0"></option>
                            <option value="1">Uno</option>
                            <option value="2">Dos</option>
                            <option value="3">Tres</option>
                            <option value="4">Cuatro</option>
                            <option value="5">Cinco</option>
                            <option value="6">Seis</option>
                        </select>
                    </div>
                    <br>
                    <br>

                    <div class="col-4">
                        <h5>Cantidad de adultos</h5>
                    </div>
                    <div class="col-8 ">
                        <select class="custom-select pull-right" name="adultos">
                            
                            <option value="1">Uno</option>
                            <option value="2">Dos</option>
                            <option value="3">Tres</option>
                            <option value="4">Cuatro</option>
                            <option value="5">Cinco</option>
                            <option value="6">Seis</option>
                        </select>
                    </div>
                    <br>
                    <br>
                    <div class="col-4">
                        <h5>Fecha de llegada </h5>
                    </div>
                    <div class="col-8">
                        <input type="date" class="form-control" name="check-in">
                    </div>
                    <br>
                    <br>
                    <div class="col-4">
                        <h5>Fecha de salida </h5>
                    </div>
                    <div class="col-8">
                        <input  type="date" class="form-control" name="check-out">
                    </div> 
                    <br>
                    <br>
                    <div class="col-4">
                        <h5>Tipo de habitación </h5>
                    </div>

                    <div class="col-8">
                        <select class="custom-select pull-right" name="tipo-habitacion">
                            <option value="Standar">Standar</option>
                            <option value="Ejecutiva">Ejecutiva</option>
                            <option value="Ejecutiva Familiar">Ejecutiva Familiar</option>
                            <option value="Suite">Suite</option>
                            <option value="Master">Master Suite</option>
                        </select>
                    </div> 
                </div>
                <br>
                    <br>
                <div class="col-md-3 offset-md-6">
                    <button type="submit" class="btn btn-outline-primary btn-lg">Reservar</button>
                </div>
               
            </form>
        </div>

        <br>
        <br>
        <br>
        <br>
        <br>
        <br>

    </body>


</html>
