<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>


    <head>
        <style>
            *{
                font-family: 'Manjari', sans-serif !important;

            }
        </style>

        <link href="https://fonts.googleapis.com/css?family=Manjari&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link type="text/css" rel="stylesheet" link="../bootstrap/css/bootstrap.min.css"/>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Recibir</title>
    </head>
    <body>
        <%@ include file="menu.jsp" %> 

        <% String adultos = request.getParameter("adultos");%>
        <% String menores = request.getParameter("menores");%>
        <% String entrada = request.getParameter("check-in");%>
        <% String salida = request.getParameter("check-out");%>
        <% String habitacion = request.getParameter("tipo-habitacion");%>

        <br>
        <br>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h3 style="color:#079992">Detalle de reserva</h3>
                    <hr>
                    <p>Menores: <%=menores%></p>
                    <p>Adultos: <%=adultos%></p>
                    <p>Entrada: <%=entrada%></p>
                    <p>Salida: <%=salida%></p>
                    <p>Habitación: <%=habitacion%></p>
                </div>
                <div class="col-md-8 ">
                    <form method="POST" action="">
                        <h3 style="text-align: center; color:#079992">Completar reserva</h3>
                        <hr>
                        <input type="text" class="form-control" placeholder="Nombre">
                        <br>
                        <input type="text" class="form-control" placeholder="Apellido">
                        <br>
                        <input type="text" class="form-control" placeholder="DNI">                    
                        <br>
                        <input type="text" class="form-control" placeholder="Dirección">
                        <br>
                        <input type="text" class="form-control" placeholder="Teléfono">
                        <br>
                        <input type="email" class="form-control" placeholder="Email">
                        <br> 
                        <textarea placeholder="Peticiones especiales" class="form-control" id="exampleFormControlTextarea1" rows="3" style="max-height:150px" maxlength="200"></textarea>
                        <br>
                        
                        <button type="submit" class="btn btn-outline-primary btn-lg">Reservar</button>
                        
                    </form>

                </div>
            </div>

        </div>
        <br>
        <br>
        <br>
    </body>
</html>



