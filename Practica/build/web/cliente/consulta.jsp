<%@include file="conexion.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      
    </head>
    <body>
        
        <sql:update dataSource="${cnx}" var="result">
            INSERT INTO consulta(nombre, apellido, email, telefono, consulta) VALUES(?,?,?,?,?);
            <sql:param value="${param.nombre}" />
            <sql:param value="${param.apellido}" />
            <sql:param value="${param.email}" />
            <sql:param value="${param.telefono}" />
            <sql:param value="${param.consulta}" />
        </sql:update>
            
            <h2>Su consulta fue enviada con éxito</h2>
            <a href="index.jsp">Regresar a la página de incio</a>
        
    </body>
</html>
